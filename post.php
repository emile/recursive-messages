<?php

    function setDeep(&$array, $path, $value) { 
        $tempArr = &$array; 
        foreach($path as $key) { 
            $tempArr = &$tempArr[$key]['messages']; 
        } 
        array_push($tempArr,$value);
    }

    $data = json_decode(file_get_contents("data/data.json"), true);

    $POST = json_decode(file_get_contents('php://input'), true);
    $POST['date'] = date("Y/m/d");
    $POST['time'] = date("H:i:s");
    $POST['messages'] = [];
    if ($POST['path']) {
        $path = $POST['path'];
        unset($POST['path']);
    }

    setDeep($data, $path, $POST);

    $data = json_encode($data, JSON_PRETTY_PRINT);
    if (file_put_contents('data/data.json', $data)) {
        echo 'sucess';
    } else {
        echo 'error';
    }
