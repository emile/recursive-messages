// cookies

cookies = {} 

cookies.set = function(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

cookies.get = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

cookies.erase = function(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

// Post

Post = function(message) {

    if (message) {
        this.message = message;
        this.message.post = this;
    }

    this.el = document.createElement('div');
    this.el.classList.add('post');
    this.text = {};
    this.text.el = document.createElement('textarea');
    this.text.el.placeholder = '...';
    this.el.appendChild(this.text.el);
    this.name = {};
    this.name.el = document.createElement('input');
    this.name.el.type = 'text';
    this.name.el.placeholder = 'Nom';
    if (cookies.get('name')) {
        this.name.el.value = cookies.get('name');
    }
    this.el.appendChild(this.name.el);
    this.mail = {};
    this.mail.el = document.createElement('input');
    this.mail.el.type = 'text';
    this.mail.el.placeholder = 'Mail';
    if (cookies.get('mail')) {
        this.mail.el.value = cookies.get('mail');
    }
    this.el.appendChild(this.mail.el);
    this.send = {};
    this.send.el = document.createElement('button');
    this.send.el.addEventListener("click", this.post.bind(this), false);
    this.send.el.innerText = 'Ok';
    this.el.appendChild(this.send.el);
}

Post.prototype.post = function () {
    var data = {};
    data.text = this.text.el.value;
    data.name = this.name.el.value;
    data.mail = this.mail.el.value;
    if (this.message) {
        data.path = this.message.path;
    }
    if (!data.text) {
        alert('text is empty');
    } else if (!data.name) {
        alert('name is empty');
    } else {
        cookies.set('name', data.name);
        cookies.set('mail', data.mail);
        session.post('post.php', JSON.stringify(data));
    }
}

// Message

Message = function(data, message, container) {

    this.parent = container;
    
    this.el = document.createElement('div');
    this.el.classList.add('message');
    this.text = {};
    this.text.el = document.createElement('div');
    this.text.el.classList.add('text');
    this.text.el.innerText = data['text'];
    this.el.appendChild(this.text.el);
    this.name = {};
    this.name.el = document.createElement('span');
    this.name.el.classList.add('name');
    this.name.el.innerText = data['name'];
    this.el.appendChild(this.name.el);
    this.el.appendChild(document.createTextNode("-"));
    this.mail = {};
    this.mail.el = document.createElement('span');
    this.mail.el.classList.add('mail');
    this.mail.el.innerText = data['mail']
    this.el.appendChild(this.mail.el);
    this.el.appendChild(document.createTextNode("-"));
    this.datetime = {};
    this.datetime.el = document.createElement('span');
    this.datetime.el.classList.add('datetime');
    this.datetime.el.innerText = data['date'] + ' ' + data['time'];
    this.el.appendChild(this.datetime.el);
    this.reply = {};
    this.reply.el = document.createElement('button');
    this.reply.el.addEventListener("click", function() {
        if (this.post) {
            this.post.text.el.focus();
        } else {
            this.post = new Post(this);
            this.el.insertBefore(this.post.el, this.container.el);
            this.post.text.el.focus();
        }
    }.bind(this))
    this.reply.el.classList.add('reply');
    this.reply.el.innerText = 'Message';
    this.el.appendChild(this.reply.el);

    this.path = [];
    if (this.parent.message) {
        this.path = this.path.concat(this.parent.message.path);
    } 
    this.path.push(message);

    this.container = new Container(data['messages'], this); 
    this.el.appendChild(this.container.el);
}

// Container

Container = function(data, message) {
    this.el = document.createElement('div');
    this.el.classList.add('container');
    
    this.message = message; 
    this.messages = [];
    for (var message in data) {
        var message = new Message(data[message], message, this);
        this.el.appendChild(message.el);
        this.messages.push(message);
    }
}

// Session

Session = function(wrapper) {
    this.wrapper = {};
    this.wrapper.el = wrapper; 
    this.rootPost = new Post();
    this.wrapper.el.appendChild(this.rootPost.el);
    this.interface();
    this.get('data/data.json');
}

Session.prototype.get = function(url) {
    fetch(url)
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            console.log(data);
            console.log(this);
            if (this.rootContainer) {
                this.rootContainer.el.remove();     
                delete this.rootContainer;
            }
            this.rootContainer = new Container(data);
            this.rootContainer.el.id = 'root';
            this.wrapper.el.appendChild(this.rootContainer.el);
        }.bind(this));
}

Session.prototype.post = function(url, data) {
    fetch(url, {
        method: 'POST',
        body: data
    })
    .then(function(response) {
        return response.text();
    })
    .then(function(data) {
        alert(data);
        this.get('data/data.json');
    }.bind(this));
}


Session.prototype.interface = function() {
    
    this.buttons = {};
    this.buttons.el = document.createElement('div');
    this.buttons.el.id = 'buttons';

    this.buttons.zoom = {};
    this.buttons.zoom.el = document.createElement('div');
    this.buttons.zoom.value = 100;

    this.buttons.zoom.in = {};
    this.buttons.zoom.in.el = document.createElement('button');
    this.buttons.zoom.in.el.innerText = '+';
    this.buttons.zoom.in.f = function () {
        if (this.buttons.zoom.value < 100) {
            this.buttons.zoom.value += 10;
            this.rootContainer.el.style.transform = 'scale('+this.buttons.zoom.value/100+')';
        }
    }.bind(this);

    this.buttons.zoom.out = {};
    this.buttons.zoom.out.el = document.createElement('button');
    this.buttons.zoom.out.el.innerText = '-';
    this.buttons.zoom.out.f = function () {
        if (this.buttons.zoom.value > 10) {
            this.buttons.zoom.value -= 10;
            this.rootContainer.el.style.transform = 'scale('+this.buttons.zoom.value/100+')';
        }
    }.bind(this);

    this.buttons.zoom.in.el.addEventListener('click', this.buttons.zoom.in.f);
    this.buttons.zoom.out.el.addEventListener('click', this.buttons.zoom.out.f);

    this.buttons.zoom.el.appendChild(this.buttons.zoom.in.el);
    this.buttons.zoom.el.appendChild(this.buttons.zoom.out.el);
    this.buttons.el.appendChild(this.buttons.zoom.el);
    this.wrapper.el.appendChild(this.buttons.el);

}

var session = new Session(document.body);
